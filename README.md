Name: Sayid Abyan Rizal Shiddiq

NPM: 1706022445

Class: B

Hobby: Playing Games

Links: 
https://gitlab.com/Say217/ap-lab
https://gitlab.com/Say217/ddp-assignment

My Notes:

git Branch & why
Branch pada git digunakan agar jika terjadi bug tidak mempengaruhi master branchnya. Sehingga master branch bisa aman dari bug dan hanya mendapat commit dari branch yang sudah bug-free.
Kenapa harus menggunakan branch? pertama misal saya mempunyai suatu proyek yang ada di master branch. lalu saya ingin melakukan perubahan yang cukup besar, jika ternyata perubahan ini mengakibatkan bug dan saya langsung commit ke master branch, master branch saya akan penuh dengan bug tersebut.

how

dari repo ddp2 yang saya clone saya menggunakan "git checkout -b "ap_lab0"" untuk membuat sekaligus checkout ke branch ap_lab0. Dengan begitu saya bisa melakukan apapun pada branch ap_lab0 tanpa mempengaruhi branch master ddp saya.

git Revert & why
Revert pada git digunakan untuk membatalkan suatu commit yang pernah dilakukan. Bisa kita lihat dari skenario tanpa branch tadi yang berakhir dengan branch master yang penuh dengan bug. Dengan meggunakan "git revert "commit hash"" untuk commit yang penuh dengan bug tersebut, branch master bisa kembali ke state sebelum commit penuh dengan bug itu ada. (commit hash bisa dilihat dengan "git log")

how
setelah membuat branch pada repo ddp saya tadi, saya me-revert commit terakhir saya dengan "git revert" lalu saya push ke proyek git saya.